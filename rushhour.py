import sys
from collections import deque
from time import sleep

CAR_IDS = {'@', 'r', 'o', 'y', 'g', 'b', 'i', 'v', 'p', 'm'}
TRUCK_IDS = {'R', 'O', 'G', 'B', 'Z'}

#############################################################

class Vehicle(object):
    
    def __init__(self, id, x, y, ori):

        self.x=x
        self.y=y
        self.id=id
        self.ori=ori

        if id in CAR_IDS:
            self.length = 2
        elif id in TRUCK_IDS:
            self.length = 3
        if ori == "H":
            x_end = self.x + (self.length - 1)
            y_end = self.y
        else:
            x_end = self.x
            y_end = self.y + (self.length - 1)

#---------------------------------------------------------------

    def __hash__(self):
        return hash(self.__repr__())
    
    def __eq__(self, other):
        return self.__dict__ == other.__dict__
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __repr__(self):
        return ("Vehicle",self.id, self.x, self.y, self.ori)

#############################################################

class RushHour(object):

    def __init__(self, vehicles):
        self.vehicles = vehicles

    def __hash__(self):
        return hash(self.__repr__())

    def __eq__(self, other):
        return self.vehicles == other.vehicles

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        s="-"*8+"\n"
        for line in self.get_board():
            s=s+"|"+(''.join(line))+"|\n"
        s=s+"-"*8+"\n"
        return s
    
    def solved(self):
        return Vehicle("@", 4, 2, "H") in self.vehicles
    
    #---------------------------------------------------------------

    def get_board(self):
        board = [[' ', ' ', ' ', ' ', ' ', ' '],
                 [' ', ' ', ' ', ' ', ' ', ' '],
                 [' ', ' ', ' ', ' ', ' ', ' '],
                 [' ', ' ', ' ', ' ', ' ', ' '],
                 [' ', ' ', ' ', ' ', ' ', ' '],
                 [' ', ' ', ' ', ' ', ' ', ' ']]
        for vehicle in self.vehicles:
            x, y = vehicle.x, vehicle.y
            if vehicle.ori == "H":
                for i in range(vehicle.length):
                    board[y][x+i] = vehicle.id
            else:
                for i in range(vehicle.length):
                    board[y+i][x] = vehicle.id
        return board

    #---------------------------------------------------------------

    def moves(self):
        board = self.get_board()
        for v in self.vehicles:
            if v.ori == "H":
                if v.x - 1 >= 0 and board[v.y][v.x - 1] == " ":
                    new_v = Vehicle(v.id, v.x - 1, v.y, v.ori)
                    new_vehicles = self.vehicles.copy()
                    new_vehicles.remove(v)
                    new_vehicles.add(new_v)
                    yield RushHour(new_vehicles)
                if v.x + v.length <= 5 and board[v.y][v.x + v.length] == " ":
                    new_v = Vehicle(v.id, v.x + 1, v.y, v.ori)
                    new_vehicles = self.vehicles.copy()
                    new_vehicles.remove(v)
                    new_vehicles.add(new_v)
                    yield RushHour(new_vehicles)
            else:
                if v.y - 1 >= 0 and board[v.y - 1][v.x] == " ":
                    new_v = Vehicle(v.id, v.x, v.y - 1, v.ori)
                    new_vehicles = self.vehicles.copy()
                    new_vehicles.remove(v)
                    new_vehicles.add(new_v)
                    yield RushHour(new_vehicles)
                if v.y + v.length <= 5 and board[v.y + v.length][v.x] == " ":
                    new_v = Vehicle(v.id, v.x, v.y + 1, v.ori)
                    new_vehicles = self.vehicles.copy()
                    new_vehicles.remove(v)
                    new_vehicles.add(new_v)
                    yield RushHour(new_vehicles)

    #---------------------------------------------------------------

    def show_pretty(self, time, c1, c2, c3):

        c=0
        
        print("---------", end="")
        print(c3, end = "")
        print(c2, end = "")
        print(c1, end = "")
        print("---------")
        
        print("X  X  X  X  X  X  X  X")
        for line in self.get_board():
            print("X  ",end="")
            for x in line:
                print(x," ",end="")
            if c==2:
                print("|")
            else:
                print("X")
            c+=1
        print("X  X  X  X  X  X  X  X\n")
        sleep(time)

        c1+=1
        if c1==10:
            c2+=1
            c1=0
        if c2==10:
            c3+=1
            c2=0
    
        return(c1, c2, c3)

#############################################################

def get_board(File):
    lines = []
    allchar = []
    vehicles=[]
    vehicles2 = []
    checker=[]
    
    for line in File:
        lines.append(line.strip("\n"))
        for char in line:
            if char !="X" and char != "\n" and char != "|":
                allchar.append(char.strip("\n"))

    #---------------------------------------------------------------

    i=0
    ti=0
    q=0
    with open("vehicles.txt", "w") as fp:
        sys.stdout = fp
        for char in allchar:
            if char != ' ':
                
                if i > 0 and char==allchar[i-1] and char not in checker:
                    checker.append(char)
                    print("{0}{1}{2}H".format(char,(i%6)-1,i//6))
                    ti=ti+1
            
                if i < 30 and char==allchar[i+6] and char not in checker:
                    checker.append(char)
                    print("{0}{1}{2}V".format(char,i%6,i//6))
                    ti=ti+1
            i=i+1

    sys.stdout = sys.__stdout__

    #---------------------------------------------------------------

    with open("vehicles.txt") as rushhour_file:

        for car in rushhour_file:
            if car.endswith("\n"):
                car = car[:-1]
            
            id, x, y, orientation = car
            vehicles2.append(Vehicle(id, int(x), int(y), orientation))

    return RushHour(set(vehicles2))

#############################################################

def breadth_first_search(r):

    states = set()
    solutions = list()
    queue = deque()
    queue.appendleft((r, tuple()))

    while len(queue) != 0:
        board, path = queue.pop()
        new_path = path + tuple([board])

        if board in states:
            continue
        else:
            states.add(board)

        if board.solved():
            solutions.append(new_path)

        else:
            queue.extendleft((move, new_path) for move in board.moves())
    
    return (states, solutions)

#############################################################

def solution_steps(solution):
    steps = []
    for i in range(len(solution) - 1):
        r1, r2 = solution[i], solution[i+1]
        v1 = list(r1.vehicles - r2.vehicles)[0]
        v2 = list(r2.vehicles - r1.vehicles)[0]
        if v1.x < v2.x:
            steps.append("{0}:>".format(v1.id))
        elif v1.x > v2.x:
            steps.append("{0}:<".format(v1.id))
        elif v1.y < v2.y:
            steps.append("{0}:˅".format(v1.id))
        elif v1.y > v2.y:
            steps.append("{0}:^".format(v1.id))
    return steps

#############################################################

showme=False

if __name__ == "__main__":
    
    speed = float(sys.argv[2])
    best = True
    J = ""
    c1 = 1
    c2 = 0
    c3 = 0
    Ans=0

    
    rushhour = get_board(open(sys.argv[1]))
    states, solutions = breadth_first_search(rushhour)
    
    #---------------------------------------------------------------

    for solution in solutions:
        Ans=Ans+1
        if best:
            
            if sys.argv[3] == "sol":
                c1 = 0
                for state in solution:
                    
                    c1, c2, c3 = RushHour.show_pretty(state, speed, c1, c2, c3)
    
            J = ", ".join(solution_steps(solution))
            Q = len(solution_steps(solution))
            best = False

        #/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\_/\

        if sys.argv[3] == "ends":
            
            for state in solution:
                if state == solution[len(solution)-1]:
                    RushHour.show_pretty(state, speed, c1, c2, c3)
                    c1+=1
        

    #---------------------------------------------------------------

    if sys.argv[3] == "states":
        for state in states:
            c1, c2, c3 = RushHour.show_pretty(state, speed, c1, c2, c3)


    print (len(states),"States Visited")

    #---------------------------------------------------------------

    if J=="":
        RushHour.show_pretty(rushhour,speed,c1, c2, c3)
        print("Unsolvable Board Configuration")

    print(Ans, "Alternative Solutions")

    if J != "":
        print("Best Solution Uses",Q ,"Moves:", J)






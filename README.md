# README #

The algorithm finds all possible solutions to the given board configuration and returns the best solution within a reasonable run time for different initial states 

TO RUN:
python3 rushhour.py sys.argv[1] sys.argv[2] sys.argv[3]

sys.argv[1] = level to solve in .txt format

sys.argv[2] = sleep speed between prints

sys.argv[3] = "sol" for solution print out, "ends" for all possible endings print out, and "states" for all possible states printed out

i.e. ("python3 rushhour.py level1.txt 0.2 sol")